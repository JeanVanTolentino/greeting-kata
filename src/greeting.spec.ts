import { Greeting } from "./greeting"

describe('greeting', ()=> {
    describe('greet', ()=> {

        let value = new Greeting;

        it('should return "Hello, Bob" when name is "Bob".', ()=> {
            let result = value.greet('Bob')
            expect(result).toEqual('Hello, Bob.')
        })
        it('should return "Hello, my friend" when name is null', ()=> {
            let result = value.greet(null)
            expect(result).toEqual('Hello, my friend.')
        })
        it('should return "HELLO JERRY!" when name JERRY is all caps.', ()=> {
            let result = value.greet('JERRY')
            expect(result).toEqual('HELLO JERRY!')
        })
        it('should return "Hello, Jill and Jane." when two names Jill and Jane are given.', ()=> {
            let result = value.greet(["Jill", "Jane"])
            expect(result).toEqual('Hello, Jill and Jane.')
        })
        it('should return "Hello, Amy, Brian, and Charlotte." when given an input of ["Amy", "Brian", "Charlotte"].', ()=> {
            let result = value.greet(["Amy", "Brian", "Charlotte"])
            expect(result).toEqual("Hello, Amy, Brian, and Charlotte.")
        })
        it('should be able to mix normal and shouted greetings.', ()=> {
            let result = value.greet(["Amy", "BRIAN", "Charlotte"])
            expect(result).toEqual("Hello, Amy and Charlotte. AND HELLO BRIAN!")
        })
        it('should split entries containing commas, returning "Hello, Bob, Charlie, and Dianne." when given ["Bob", "Charlie, Dianne"]', ()=> {
            let result = value.greet(["Bob", "Charlie, Dianne"])
            expect(result).toEqual("Hello, Bob, Charlie, and Dianne.")
        })
        it('should recognize intentional commas, returning "Hello, Bob and Charlie, Dianne." when given ["Bob", "\"Charlie, Dianne\""]', ()=> {
            let result = value.greet(["Bob", "\"Charlie, Dianne\""])
            expect(result).toEqual("Hello, Bob and Charlie, Dianne.")
        })
    })
})