export class Greeting {
    greet(name:string|string[]){
        if(name){
            if(Array.isArray(name)){

                name = name.map(a=>{
                    if(!/"/.test(a)){
                        return a.split(', ')
                    } else {
                        return a.replace(/"/g, '')
                    }
                }).flat()

                let allCaps=[]
                let allSmall=[]
                let joined = ''
                
                for(let i=0; name.length>i; i++){
                    if(name[i].toUpperCase()===name[i]){
                        allCaps.push(name[i])
                    } else {
                        allSmall.push(name[i])
                    }
                }

                if(allSmall.length>2){
                    for(let i=0; allSmall.length>i; i++){
                        if(allSmall.length-1>i){
                            joined += allSmall[i] + ', '
                        } else {
                            joined += 'and ' + allSmall[i]
                        }
                    }
                    joined = `Hello, ${joined}.`
                } else if (allSmall.length===2){
                    joined = `Hello, ${allSmall.join(' and ')}.`
                } else if (allSmall.length===1){
                    joined = `Hello, ${allSmall[0]}.`
                }

                if(allSmall.length>0 && allCaps.length>0){
                    joined = joined + ' AND HELLO '
                }

                if(allCaps.length>2){
                    for(let i=0; allCaps.length>i; i++){
                        if(allCaps.length-1>i){
                            joined += allCaps[i] + ', '
                        } else {
                            joined += 'AND ' + allCaps[i]
                        }
                    }
                    joined = `${joined}!`
                } else if (allCaps.length===2){
                    joined += allCaps.join(' AND ') + '!'
                } else if (allCaps.length===1){
                    joined += `${allCaps[0]}!`
                }
                return joined
            } else {
                if(name.toUpperCase()===name){
                    return `HELLO ${name}!`
                } else {
                    return `Hello, ${name}.`
                }
            }
        } else {
            return 'Hello, my friend.'
        }
    }
}